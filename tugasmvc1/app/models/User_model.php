<?php 

class User_model{
    private $model;

    public function __construct($dbh) {
        $this->model = new User_model($dbh);
    }

    public function showLoginForm() {
        require_once 'views/login.php';
    }

    public function login() {
        if (isset($_POST['submit'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if ($this->model->login($username, $password)) {
                echo "<script>window.location = 'index.php'</script>";
            } else {
                echo "<script>alert('Login failed. Invalid username or password.');</script>";
                $this->showLoginForm();
            }
        } else {
            $this->showLoginForm();
        }
    }

    public function logout() {
        $this->model->logout();
        echo "<script>window.location = 'index.php'</script>";
    }
}