<?php 

class Tabelmakanan_model {
    private $dbh;
    private $stmt;

    public function __construct(){
        //data source name
        $dsn = 'mysql:host=localhost;dbname=test';

        try{
            $this->dbh = new PDO($dsn, 'root', '');
        }catch(PDOException $e){
            die($e->getMessage());
        }
    }

    public function getAllTabelMakanan(){
        $this->stmt = $this->dbh->prepare('SELECT * FROM tabelresep');
        $this->stmt->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insertResep($data, $file) {
        // Proses upload file
        $name = $file['name'];
        $tmp_name = $file['tmp_name'];
        move_uploaded_file($tmp_name, 'upload/' . $name);

        // Proses insert
        $query_insert = 'INSERT INTO tabelresep (nama_resep, deskripsi, alat, bahan, step, kategori, foto) VALUES (?, ?, ?, ?, ?, ?, ?)';
        $stmt = $this->dbh->prepare($query_insert);
        return $stmt->execute([
            $data['nama'],
            $data['deskripsi'],
            $data['alat'],
            $data['bahan'],
            $data['step'],
            $data['kategori'],
            $name
        ]);
    }

    public function deleteResep($id) {
        $query_delete = 'DELETE FROM tabelresep WHERE id_resep = ?';
        $stmt = $this->dbh->prepare($query_delete);
        return $stmt->execute([$id]);
    }
}