<style>
	.fa-solid:hover{
		color: #627254;
	}
	.fa-solid{
		color: #8ca16d;
	}
	.card{
		background-color: white;
		margin-top: 20px;
	}
	.column{
		float: left;
		padding-left: 20px;
	}
	.icon{
		width: 50px;
		margin: 0px 5px 0px 5px;
	}
</style>
</head>
<body>
<!-- Selamat datang <?php echo $_SESSION["user"]; ?>, <br> -->
	<div class="row">
	<div class="column">
		<h2>Daftar Resep</h2>
	</div>
	<div class="column"> <a href="resep_add.php" align="center" title="Posting resep baru"><i class="fa-solid fa-square-plus fa-2x"></i></i></a></div>
    <br>
	</div>
	<div class="card">
	<table border="1" cellpadding="25" cellspacing="0" width="">

	<tr>
		<th align="center">No.</th>
		<th>Nama Resep</th>
		<th>Deskripsi</th>
		<th>Alat</th>
		<th>Bahan</th>
		<th>Step</th>
		<th>Kategori</th>
        <th>Foto</th>
        <!-- <th>Waktu Upload</th> -->
		<th>Edit</th>
	</tr>

	
	<?php $nomor = 1;?>
	<?php foreach($data['makanan'] as $mkn) :?>
	<tr>
		<td><?=$nomor++ ?></td>
		<td><?= $mkn['nama_resep']?></td>
		<td><?= $mkn['deskripsi']?></td>
		<td><?= $mkn['alat']?></td>
		<td><?= $mkn['bahan']?></td>
		<td><?= $mkn['step']?></td>
		<td><?= $mkn['kategori']?></td>
		<td align="center"><img src="../upload/<?= $mkn['7']?>" width="90%" height="90%"></td>
		<!-- <td><?= $mkn['foto']?></td> -->
		<td align="center" >
			<div class="icon">
		<a href="resep_edit.php?id=<?= $mkn["id_resep"]; ?>" title="Ubah data" class=""><i class="fa-solid fa-square-pen"></i>&nbsp;|</a> 
		<a href="?delete=<?= $mkn["id_resep"] ?>" onclick="return confirm('Apakah Anda yakin untuk menghapus item ini?');" title="Hapus data"><i class="fa-solid fa-trash"></i></a>
			</div>
		</td>
	</tr>
	
	<?php endforeach;?>



	
	</table>
	</div>
