
        <style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans;wght@300;400;700&display=swap');
            * {
                padding: 0;
                margin: 0;
            }
            body {
                font-family: 'Nunito Sans', sans-serif;
                background-color: #FEFAF6;
            }
            a {
                color: inherit;
                text-decoration: none;
            }
            .container {
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .card{
                margin-top: -30px;
                border: 1px solid;
                background-color: #fff;
                width: 650px;
                padding: 25px 15px;
                box-sizing: border-box;
                border-radius: 5px;
            }
            .card h3 {
                margin-bottom: 10px;
            }
            .input-group {
                margin-bottom: 10px;
            }
            .input-control {
                width: 100%;
                display: block;
                padding: 0.5rem 1rem;
                box-sizing: border-box;
                font-size: 1rem;
                margin-bottom: 8px;
            }
            .btn-submit {
                display: inline-block;
                width: auto;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                background-color: #8ca16d;
                color: black;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn-back {
                display: inline-block;
                width: auto;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn-back:hover{
                background-color: #627254;
                color: #fff;
            }
            .btn-submit:hover {
                background-color: #627254;
                color: #fff;
            }
        </style>
        <script src = "https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.min.css">
    </head>
    <body>
<div class="content">
    <div class="container">
        <div class="card">

        <h3 class="page-title">Edit Resep</h3>

            <form action="" method="post" enctype="multipart/form-data">
                <div class="input-group">
                    <label>Nama Resep</label>
                    <input type="text" name="nama" placeholder="Nama resep" id="nama" class="input-control" value="<?= $d->nama_resep?>">
                </div>

                <div class="input-group">
                    <label>Deskripsi</label>
                    <textarea type="range" class="input-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi""><?php print_r($d->deskripsi)?></textarea>
                    
                </div>

                <div class="input-group">
                    <label>Alat</label>
                    <textarea class="input-control" name="alat" id="alat" placeholder="Alat" ><?php print_r($d->alat)?></textarea>
                </div>

                <div class="input-group">
                    <label>Bahan</label>
                    <textarea class="input-control" name="bahan" id="bahan" placeholder="Bahan"><?php print_r($d->bahan)?></textarea>
                </div>

                <div class="input-group">
                    <label>Step</label>
                    <textarea class="input-control" name="step" id="step" placeholder="Step"><?php print_r($d->step)?></textarea>
                </div>

                <div class="input-group">
                    <label>Kategori</label>
                    <select class="input-control" id="kategori" name="kategori" required>
                        <option value="">Pilih</option>
                        <option value="Makanan Berat">Makanan Berat</option>
                        <option value="Camilan">Camilan</option>
                        <option value="Kue">Kue</option>
                        <option value="Minuman">Minuman</option>
                    </select>
                </div>

                <div class="input-group">
                    <label>Foto</label>
                    <input type="file" id="foto" name="foto" required>
                </div>

                <div class="input-group">
                    <button type="button" onclick="window.location.href ='tabelmakanan.php'" class="btn-back">Batalkan</button>
                    <button type="submit" id= "simpan" name="submit" class="btn-submit">Simpan</button>
                </div>

            </form>