<style type="text/css">
        /* detail content */
        .container {
            width : 900px;
            margin-left : auto;
            margin-right : auto;
        }
        .card-resep{
            margin-top: 15px;
            background-color: white;
            padding: 25px 65px 0px 40px;
            border: 1px solid;
            border-radius: 20px;
        }
        .card-resep img {
            width : 100%;
            height: 350px;
            object-fit: fill;
            border-radius: 30px;
            padding: 10px 10px 10px 10px;
        }
        .card-body {
            padding : 15px;
        }
        .card-judul{
            margin-left: 50px;
        }
        .resep-kategori {
            display: flex;
            justify-content: center;
            align-items: center; /* Mengatur vertikal alignment */
            margin-bottom: 13px;
        }
        .isi-kategori {
            padding: 5px 10px;
            border-radius: 5px;
            background-color: #627254;
            margin-right: 15px;
            margin-left: 15px;
            color: #fff;
        }
        .resep-nama {
            text-align: center;
            font-size: 32px;
            font-weight: bold;
            margin-bottom: 8px;
        }
        .resep-deskripsi {
            text-align: center;
            font-size: 14px;
            color : #627254;
            margin-bottom: 20px;
        }
        .resep-alat {
            margin-bottom: 10px;
        }
        .resep-bahan {
            margin-bottom: 10px;
        }
        .resep-step {
            margin-bottom: 10px;
        }
        @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans;wght@300;400;700&display=swap');
        
            .card{
                margin-top: -30px;
                width: 900px;
                height: 200px;
                padding: 25px 15px;
                border: 1px solid;
                box-sizing: border-box;
                border-radius: 20px;
            }
            .card h4 {
                margin-bottom: 10px;
            }
            .input-group {
                margin-bottom: 10px;
            }
            .input-control {
                width: 100%;
                display: block;
                padding: 0.5rem 1rem;
                box-sizing: border-box;
                font-size: 1rem;
                margin-bottom: 8px;
            }
            .btn-submit {
                /* float: right; */
                display: inline-block;
                width: auto;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                background-color: #8ca16d;
                color: black;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn-submit:hover {
                background-color: #627254;
                color: #fff;
            }
    </style>

        <script src = "https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.min.css">
</head>
<body>
    <!--detail content-->
    <div class="container">
        <div class="card-resep">
            <img src="https://www.kitchensanctuary.com/wp-content/uploads/2020/07/Nasi-Goreng-square-FS-57.jpg">

            <div class="card-body">
                <div class="card-judul">
                    <div class="resep-kategori">
                        <div class="isi-kategori">Makanan Berat</div>
                        <div class="isi-kategori">30 Menit</div>
                    </div>
                    <div class="resep-nama">Nasi Goreng Spesial</div>
                    <div class="resep-deskripsi">
                        <p>Nasi goreng yang lezat dengan bumbu spesial</p>
                        Siapa bilang nasi goreng harus biasa-biasa saja? Nasi goreng kami menghadirkan sensasi lezat yang bikin lidah bergoyang! 
                        Dibuat dengan cinta dan bumbu-bumbu pilihan, setiap suapan adalah perjalanan rasanya yang seru. 
                        Dari butir nasi yang renyah hingga potongan sayuran yang segar, semuanya dipadu dengan sentuhan rahasia kami yang membuat nasi goreng ini juara. 
                        Lezatnya nasi goreng gaul ini siap memuaskan selera tanpa harus repot memasak!</div>
                </div>
            
                    <div class="resep-alat">
                        <b>Alat :</b>
                        <ul>
                            <li>Wajan</li>
                            <li>Sendok</li>
                        </ul>
                    </div>
                    <div class="resep-bahan">
                        <b>Bahan :</b>
                        <ul>
                            <li>300g nasi putih</li>
                            <li>100g daging ayam/sapi/udang</li>
                            <li>2 butir telur</li>
                            <li>1 buah bawang merah</li>
                            <li>2 siung bawang putih</li>
                            <li>1 batang daun bawang</li>
                            <li>2 sdm kecap manis</li>
                            <li>1 sdm saus tiram</li>
                            <li>1 sdm minyak wijen</li>
                    </div>
                    <div class="resep-step">
                        <b>Langkah - Langkah :</b>
                        <ol>
                            <li>Tumis bawang merah dan bawang putih hingga harum.</li>
                            <li>Masukkan daging, telur, dan nasi, aduk rata.</li>
                            <li>Tambahkan kecap manis, saus tiram, dan minyak wijen, aduk hingga merata.</li>
                            <li>Taburkan daun bawang, aduk sebentar, lalu angkat.</li>
                        </ol>
                    </div>

            </div>
        </div>
    </div>

    <!--comment-->
<div class="content">
    <div class="container">
        <div class="card">
        <h4 class="page-title">Komentar</h4>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="input-group">
                    <textarea class="input-control" id="comment" name="comment" placeholder="Komentar" required></textarea>
                </div>

                <div class="input-group">
                    <button type="submit" id ="unggah" name="submit" class="btn-submit">Unggah</button>
                </div>

            </form>

            <script>
    document.getElementById('unggah').addEventListener('click', function() {
        var komen = document.getElementById('comment').value;
        
        var formData = new FormData();
        formData.append('comment', komen);

        axios.post('detail.php', formData)
    });
</script>
        </div>

    </div>

</div>

<div class="content">
    <div class="container">
        <div class="card">
        <?php 
    if (isset($_POST["comment"])){
        ?>
        <table border=0 style="width:100%">
            <tr>
                <th></th>
            </tr>
            <tr>
                <td>
                    <?php echo $_POST["comment"];?>
                </td>
            </tr>
        </table>
        <?php
    } ?>

        </div>
    </div>
</div>
