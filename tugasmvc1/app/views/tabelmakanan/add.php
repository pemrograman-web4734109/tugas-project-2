<style type="text/css">
            @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans;wght@300;400;700&display=swap');
            * {
                padding: 0;
                margin: 0;
            }
            body {
                font-family: 'Nunito Sans', sans-serif;
            }
            a {
                color: inherit;
                text-decoration: none;
            }
            .container {
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .card{
                margin-top: -30px;
                border: 1px solid;
                background-color: #fff;
                width: 650px;
                padding: 25px 15px;
                box-sizing: border-box;
                border-radius: 5px;
            }
            .card h3 {
                margin-bottom: 10px;
            }
            .input-group {
                margin-bottom: 10px;
            }
            .input-control {
                width: 100%;
                display: block;
                padding: 0.5rem 1rem;
                box-sizing: border-box;
                font-size: 1rem;
                margin-bottom: 8px;
            }
            .btn-submit {
                display: inline-block;
                width: auto;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                background-color: #8ca16d;
                color: black;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn-back {
                display: inline-block;
                width: auto;
                padding: 0.5rem 1rem;
                cursor: pointer;
                font-size: 1rem;
                margin-top: 8px;
                border-radius: 5px;
                border-color: #627254;
                outline-color: #8ca16d;
            }
            .btn-back:hover{
                background-color: #627254;
                color: #fff;
            }
            .btn-submit:hover {
                background-color: #627254;
                color: #fff;
            }
           
        </style>
        <script src = "https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.7.5/dist/sweetalert2.min.css">
    </head>
    <body>
<div class="content">
    <div class="container">
        <div class="card">
        <h3 class="page-title">Upload Resep</h3>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="input-group">
                    <label>Nama Resep</label>
                    <input type="text" id="nama" name="nama" placeholder="Nama resep" class="input-control" required>
                </div>

                <div class="input-group">
                    <label>Deskripsi</label>
                    <textarea class="input-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi" required></textarea>
                </div>

                <div class="input-group">
                    <label>Alat</label>
                    <textarea class="input-control" id="alat" name="alat" placeholder="Alat" required></textarea>
                </div>

                <div class="input-group">
                    <label>Bahan</label>
                    <textarea class="input-control" id="bahan" name="bahan" placeholder="Bahan" required></textarea>
                </div>

                <div class="input-group">
                    <label>Step</label>
                    <textarea class="input-control" id="step" name="step" placeholder="Step" required></textarea>
                </div>

                <div class="input-group">
                    <label>Kategori</label>
                    <select class="input-control" id="kategori" name="kategori">
                        <option value="">Pilih</option>
                        <option value="Makanan Berat">Makanan Berat</option>
                        <option value="Camilan">Camilan</option>
                        <option value="Kue">Kue</option>
                        <option value="Minuman">Minuman</option>
                    </select>
                </div>

                <div class="input-group">
                    <label>Foto</label>
                    <input type="file" id="foto" name="foto" required>
                </div>

                <div class="input-group">
                    <button type="button" onclick="window.location.href ='beranda/beranda.php'" class="btn-back">Kembali</button>
                    <button type="submit" id ="unggah" name="submit" class="btn-submit">Unggah</button>
                </div>

            </form>

            <script>
    document.getElementById('unggah').addEventListener('click', function() {
        var nama = document.getElementById('nama').value;
        var deskripsi = document.getElementById('deskripsi').value;
        var alat = document.getElementById('alat').value;
        var bahan = document.getElementById('bahan').value;
        var step = document.getElementById('step').value;
        var kategori = document.getElementById('kategori').value;
        var foto = document.getElementById('foto').files[0];
        
        var formData = new FormData();
        formData.append('nama', nama);
        formData.append('deskripsi', deskripsi);
        formData.append('alat', alat);
        formData.append('bahan', bahan);
        formData.append('step', step);
        formData.append('kategori', kategori);
        formData.append('foto', foto);

        axios.post('resep_add.php', formData)
    });
</script>

            <?php
                if(isset($_POST['submit'])){
                    //proses insert data resep

                    //tampung data file yang akan diupload
                    $name = $_FILES['foto']['name'];
                    $tmp_name = $_FILES['foto']['tmp_name'];

                    // proses upload filenya
                    move_uploaded_file($tmp_name, 'upload/' . $name);

                    // proses insert
                    $query_insert = 'insert into tabelresep
                    (nama_resep, deskripsi, alat, bahan, step, kategori, foto) value (
                        "'.$_POST['nama'].'",
                        "'.$_POST['deskripsi'].'",
                        "'.$_POST['alat'].'",
                        "'.$_POST['bahan'].'",
                        "'.$_POST['step'].'",
                        "'.$_POST['kategori'].'",
                        "'.$name.'" 
                        )';

                    
                    $run_query_insert = mysqli_query($conn, $query_insert);

                    if($run_query_insert){
                        echo "<script>
                        Swal.fire({
                            icon: 'success',
                            title: 'Unggahan Berhasil!',
                            text: 'Resep anda telah berhasil diunggah, anda dapat melihat unggahan anda pada menu Daftar Resep',
                            showConfirmButton: true,
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location = 'tabelmakanan.php';
                            }
                        });</script>";
                       
                    }else{
                        echo" <script>
                        .catch(function(error) {
                            Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Terjadi kesalahan saat mengunggah resep.',
                            showConfirmButton: true,
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location = 'tabelmakanan.php'; // Redirect ke tabelmakanan.php jika tombol OK diklik
                            }
                        });</script>" .mysqli_error($conn);
                    }
                }
            ?>

        </div>

    </div>

</div>