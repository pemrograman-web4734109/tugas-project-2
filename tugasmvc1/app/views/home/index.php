

    <style type="text/css">
      html {
  box-sizing: border-box;
  font-size: 100%;
}

html,
body {
  height: 100%;
}

.container{
    height:80px;
    width:1000px;
}

.container-rekom{
  margin-top: -30px;
  margin-bottom: -50px;
}

*, *:before, *:after {
  box-sizing: inherit;
}

img {
  max-width: 100%;
  height: auto;
}

body {
  -webkit-text-size-adjust: 100%;
  font-variant-ligatures: none;
  text-rendering: optimizeLegibility;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  font-size: 100%;
}

h1,
h2,
h3,
h4,
h5 {
  font-weight: 800;
  margin-top: 0;
  margin-bottom: 0;
}

body {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0;
  padding: 0;
  height: 100vh;
  color: #1F1D42;
  background-color: #F0F8E1;
}

.col-md-4 {
  float: left;
  width: 336px;
  padding: 0 10px;
}

/* Remove extra left and right margins, due to padding in columns */
.row {
  margin: 0 -5px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

.card-hover {
  margin-top: 80px;
  width: 300px;
  height: 500px;
  border-radius: 10px;
  position: relative;
  overflow: hidden;
  box-shadow: 0 0 32px -10px rgba(0, 0, 0, 0.08);
}
.card-hover:has(.card-hover__link:hover) .card-hover__extra {
  transform: translateY(0);
  transition: transform 0.35s;
}
.card-hover:hover .card-hover__content {
  background-color: #DEE8C2;
  bottom: 100%;
  transform: translateY(100%);
  padding: 60px 40px;
  transition: all 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
}
.card-hover:hover .card-hover__link {
  opacity: 1;
  transform: translate(-50%, 0);
  transition: all 0.3s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
}
.card-hover:hover img {
  transform: scale(1);
  transition: 0.35s 0.1s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
}
.card-hover__content {
  width: 100%;
  text-align: center;
  background-color: #8ca16d;
  padding: 0 60px 50px;
  position: absolute;
  bottom: 0;
  left: 0;
  transform: translateY(0);
  transition: all 0.35s 0.35s cubic-bezier(0.1, 0.72, 0.4, 0.97);
  will-change: bottom, background-color, transform, padding;
  z-index: 1;
}
.card-hover__content::before, .card-hover__content::after {
  content: "";
  width: 100%;
  height: 90px;
  background-color: inherit;
  position: absolute;
  left: 0;
  z-index: -1;
}
.card-hover__content::before {
  top: -80px;
  -webkit-clip-path: ellipse(60% 60px at bottom center);
          clip-path: ellipse(60% -80px at bottom center);
}
.card-hover__content::after {
  bottom: -50px;
  -webkit-clip-path: ellipse(60% 80px at top center);
          clip-path: ellipse(60% 80px at top center);
}
.card-hover__title {
  font-size: 1.5rem;
  margin-bottom: 1em;
}
.card-hover__title span {
  color: #2d7f0b;
}
.card-hover__text {
  font-size: 0.75rem;
}
.card-hover__link {
  position: absolute;
  bottom: 1rem;
  left: 50%;
  transform: translate(-50%, 10%);
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  text-decoration: none;
  color: #2d7f0b;
  opacity: 0;
  padding: 10px;
  transition: all 0.35s;
}
.card-hover__link:hover svg {
  transform: translateX(4px);
}
.card-hover__link svg {
  width: 18px;
  margin-left: 4px;
  transition: transform 0.3s;
}
.card-hover__extra {
  height: 55%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  font-size: 1.5rem;
  text-align: center;
  background-color: #8ca16d;
  padding: 50px;
  bottom: -20px;
  z-index: 0;
  color: #dee8c2;
  transform: translateY(100%);
  will-change: transform;
  transition: transform 0.35s;
}
.card-hover__extra span {
  color: #2d7f0b;
}
.card-hover img {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
     object-fit: cover;
  -o-object-position: center;
     object-position: center;
  z-index: -1;
  transform: scale(1.2);
  transition: 0.35s 0.35s transform cubic-bezier(0.1, 0.72, 0.4, 0.97);
}
        body, html {
          height: auto;
          margin: 0;
          font-family: 'Nunito Sans', Arial, Helvetica, sans-serif;
          background-color: #FEFAF6;
        }
        *{
          padding: 0;
          margin: 0;
        }
        a {
          color: inherit;
          text-decoration: none;
        }
    </style>
</head>
<body>
  <div class="content">
    <div class="container" style="margin-top: 5px;">
        <div class="card" style="background-color: white">
            <h2>Hai <?= $data['nama'];  ?></h2> 
            <p>Selamat datang di website yang menyediakan berbagai macam resep makanan</p>
        </div>
    </div>
  </div>
  
  <div class="container-rekom">
      <div class="row text-center p-4">
        <div class="col-12">
          <h2 class="font-wigth-light">Rekomendasi</h2>
        </div>
      </div>
    </div>

    <div class="container text-center my-3">
      <div class="row mx-auto my-auto justify-content-center">
          <div class="col-md-4">
              <div class="card-hover">
                <div class="card-hover__content">
                    <h3 class="card-hover__title"> Nasi Goreng Jawa <span>Spesial</span></h3>
                    <p class="card-hover__text">Nasi goreng yang lezat dengan bumbu spesial</p>
                    <a href="../detail.php" class="card-hover__link">
                      <span>Lihat Resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://www.kitchensanctuary.com/wp-content/uploads/2020/07/Nasi-Goreng-square-FS-57.jpg" alt="">
                </div>
            </div>

            <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Pisang Goreng <span>Super</span> Crispy
                    </h3>
                    <p class="card-hover__text">Pisang goreng yang renyah diluar dan manis di dalam</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://1.bp.blogspot.com/-XVE7DSE1LGg/TZ7b0j1GVnI/AAAAAAAAACs/3doaa5Rl-ps/s1600/pisang_goreng.jpg" alt="">
              </div>   
          </div>
          
          <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Kepiting <span>Saos</span> Padang
                    </h3>
                    <p class="card-hover__text">Daging kepiting yang lembut dan manis sangat lezat dimasak dengan saos padang yang gurih</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://th.bing.com/th/id/OIP.uO9NKFOwKgVr8bRDS0eGeQHaJ4?w=675&h=900&rs=1&pid=ImgDetMain" alt="">
              </div>
          </div>

          <div class="col-md-4">
              <div class="card-hover">
                <div class="card-hover__content">
                    <h3 class="card-hover__title"> Tanghulu <br><span>Spesial</span></h3>
                    <p class="card-hover__text">Tanghulu dari buah segar yang membuat ketagihan</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat Resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="/upload/tanghulu-recipe-IG.jpg" alt="">
                </div>
            </div>

            <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Brownis <span>Kukus</span> Cokelat
                    </h3>
                    <p class="card-hover__text">Brownis Kukus Rasa Cokelat yang lembut dan empuk</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="https://th.bing.com/th/id/OIP.o3Wd3dK0MwKmW1TOEvV3YgHaE8?rs=1&pid=ImgDetMain" alt="">
              </div>   
          </div>
          
          <div class="col-md-4">
              <div class="card-hover">
                 <div class="card-hover__content">
                    <h3 class="card-hover__title">
                    Es <span>Buah</span> <br>Bakak
                    </h3>
                    <p class="card-hover__text">Buah-buahan yang segar dicampur dengan susu dan sirup sangat nikmat</p>
                    <a href="#" class="card-hover__link">
                      <span>Lihat resep</span>
                      <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3" />
                      </svg>        
                    </a>
                  </div>
                  <div class="card-hover__extra">
                    <h4>Lihat resep <span>Sekarang</span> Recook dan <span>buktikan</span> rasanya!</h4>
                  </div>
                  <img src="/upload/es_buah.jpg" alt="">
              </div>
          </div>

          
          


      </div>
    </div>


