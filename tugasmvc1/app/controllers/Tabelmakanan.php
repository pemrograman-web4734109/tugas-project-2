<?php 

class Tabelmakanan extends Controller{
    public function index(){
        $data['makanan'] = $this->model('Tabelmakanan_model')->getAllTabelMakanan();
        $this->view('templates/header');
        $this->view('tabelmakanan/index', $data);
        $this->view('templates/footer');
    }

    public function detail(){
        $data['makanan'] = $this->model('Tabelmakanan_model')->getAllTabelMakanan();
        $this->view('templates/header');
        $this->view('tabelmakanan/detail', $data);
        $this->view('templates/footer');
    }

    public function add(){
        $data['makanan'] = $this->model('Tabelmakanan_model')->getAllTabelMakanan();
        $this->view('templates/header');
        $this->view('tabelmakanan/add', $data);
        $this->view('templates/footer');
    }

    public function delete($id) {
        $result = $this->model('Tabelmakanan_model')->deleteResep($id);
        if ($result) {
            echo "<script>window.location = 'tabelmakanan.php'</script>";
        } else {
            echo "<script>alert('Data gagal dihapus')</script>";
        }
    }

    public function insert() {
        if (isset($_POST['submit'])) {
            $data = [
                'nama' => $_POST['nama'],
                'deskripsi' => $_POST['deskripsi'],
                'alat' => $_POST['alat'],
                'bahan' => $_POST['bahan'],
                'step' => $_POST['step'],
                'kategori' => $_POST['kategori']
            ];
            $file = $_FILES['foto'];
            $result = $this->model('Tabelmakanan_model')->insertResep($data, $file);
            if ($result) {
                echo "<script>window.location = 'tabelmakanan.php'</script>";
            } else {
                echo "<script>alert('Data gagal ditambahkan')</script>";
            }
        } else {
            
        }
    }
}